class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy # if post deleted, delete all comments associated
	validates_presence_of :title
	validates_presence_of :body
end
